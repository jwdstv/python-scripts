import os,errno, urllib2, xml.etree.ElementTree as ET, shutil, argparse

#remove a file if exist - dont throw exception
def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e: 
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured

#parse and set arguments including defaults
def parse_arguments():	
	parser = argparse.ArgumentParser()
	parser.add_argument("-mr","--maven_repo",help="the url of the maven repository",default="http://197.80.203.140:8081/nexus/content/repositories/releases/com/dstvo/api/cs/")
	parser.add_argument("-aid","--artifact_id",help="the maven artifact_id according to pom",default="BasicSchedule")
	parser.add_argument("-mmf","--metadata_filename",help="the maven metadata file",default="maven-metadata.xml")
	parser.add_argument("-adn","--app_deploy_name",help="optional new name of application")
	parser.add_argument("-tad","--tomcat_app_folder",help="the tomcat webapps folder where war will be deployed",default="/srv/tomcat/dcs/webapps/")
	parser.add_argument("-rv","--release_version",help="the version to be deployed")
	args = parser.parse_args()
	return args

#get maven meta file and from there the release verion
def retrieve_latest_release_version_from_maven():
	if not args.release_version:
		maven_metadata_loc = args.maven_repo + args.artifact_id + '/' + args.metadata_filename
		#print 'fetching maven meta file:' + maven_metadata_loc	
		response = urllib2.urlopen(maven_metadata_loc)
		metadata_file = response.read()
		root = ET.fromstring(metadata_file)
		release_version = str(root.find("versioning/release").text)
	else:
		release_version = args.release_version
	return release_version

# get the actual built war file from the maven repo 
def retrieve_war_file_from_maven(maven_repo, artifact_id, release_version, war_filename):
	war_location = maven_repo + artifact_id + "/" + release_version + "/" + war_filename	
	response = urllib2.urlopen(war_location)
	war_file = response.read()
	return war_file

# save the war file as tmp
def save_temp_file(war_file, war_filename):
	war_filename_temp = 'tmp_' + war_filename
	with open(war_filename_temp, "wb") as code:
		code.write(war_file)
	return war_filename_temp

# get the actual built war file from the maven repo and save as tmp
def download_and_save_temp_file(maven_repo, artifact_id, release_version, war_filename):	
	war_file = retrieve_war_file_from_maven(maven_repo, artifact_id, release_version, war_filename)
	war_filename_temp = save_temp_file(war_file, war_filename)			
	return war_filename_temp

# copy file to tomcat deploy folder
def deploy_war(tomcat_dest_folder, war_filename_temp, war_filename, app_deploy_name):
	if app_deploy_name:
		dst_warfile = tomcat_dest_folder + app_deploy_name + '.war'
	else:
		dst_warfile = tomcat_dest_folder + war_filename
	silentremove(dst_warfile)
	shutil.copyfile(war_filename_temp, dst_warfile)
	#remember to delete tmp_version
	#print "deployed to " + str(dst_warfile)



args = parse_arguments()
release_version = retrieve_latest_release_version_from_maven()
war_filename = args.artifact_id + "-" + release_version + '.war'
war_file = retrieve_war_file_from_maven(args.maven_repo, args.artifact_id, release_version, war_filename)
war_filename_temp = save_temp_file(war_file, war_filename)
deploy_war(args.tomcat_app_folder, war_filename_temp, war_filename, args.app_deploy_name)